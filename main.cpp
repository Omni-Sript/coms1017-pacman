#include <iostream>
#include <thread>
#include <chrono>

#include "window.h"
#include "texture.h"
#include "tile.h"
#include "helpers.h"
#include "character.h"

using namespace std;

int main()
{
    // SpriteSheet Filename
    string spriteFilename = SPRITEFILENAME; // Leave this line
    Texture myTexture;
    myTexture.loadFile(spriteFilename,20,20);

    //Tile pm(0,0,{{1,1},{1,2},{1,1},{1,3}},Pacman,1,1);
    Character myPacman(0,0,Pacman);
    // Setup and Load Texture object here
    bool quit = false;
    //vector<Tile> testTiles = getTestTiles();
  int frame=0;
    while(!quit){
        // Handle any SDL Events
        SDL_Event e;
       while (SDL_PollEvent(&e)){
        if(e.type == SDL_QUIT){

            quit=true;
        }
        myPacman.handle_event(e);
       }

       //Set background to black
  SDL_SetRenderDrawColor(myTexture.myWin.sdlRenderer, 0, 0, 0, 0xFF);


       //Clear the Renderer
         SDL_RenderClear(myTexture.myWin.sdlRenderer);

         //Render the tile
     myPacman.render(&myTexture, frame);


         //Loop for the testtiles
           // for(int i=0; i<testTiles.size();i++){
           //         testTiles[i].render(&myTexture, frame);
           // }

     //Copy the memory of the renderer to the window
      SDL_RenderPresent(myTexture.myWin.sdlRenderer);

     frame++;
        // myPacman.render(&myTexture, frame);

      this_thread::sleep_for(chrono::milliseconds(75));

        // Such as resize, clicking the close button,
        //  and process and key press events.

        // Update the Game State Information

        // Draw the current state to the screen.
    }




    return 0;
}
