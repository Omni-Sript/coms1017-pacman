#include "tile.h"

int Tile::tileWidth = -1;
int Tile::tileHeight = -1;

/**
 * @brief Tile::Tile Constructs the tile object.
 * @param windowX X co-ordinate (in pixels) of the top left corner for rendering.
 * @param windowY Y co-ordinate (in pixels) of the top left corner for rendering.
 * @param frames Vector of {row, col} co-ordinates for images in the animation.
 * @param t The type of tile.
 * @param spriteWidth The width of the animation in tiles (usually 1).
 * @param spriteHeight The height of the animation in tiles (usually 1).
 */
Tile::Tile(int windowX, int windowY, std::vector<std::pair<int, int> > frames, TileType t, int spriteWidth, int spriteHeight)
{
 myType = t;
 myFrames=frames;
 x=windowX;
 y=windowY;
 w=spriteWidth;
 h=spriteHeight;

// int x = spriteWidth*myFrames[i].first;
// int y = spriteHeight*myFrames[i].second;
// int w = windowX*spriteWidth;
// int h = windowY*spriteHeight;


 //SDL_Rect a = {x, y, w, h};
// return(a);


}
/**
 * @brief Tile::render Uses the texture object to render a frame from the current tile.
 * @param t The Texture object to use.
 * @param frame The frame % numFrames that should be rendered.
 */
void Tile::render(Texture *t, int frame)
{
    int i = frame % myFrames.size();
t->render(x, y, myFrames[i].first, myFrames[i].second, w, h);

}
