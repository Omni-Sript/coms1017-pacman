#include "character.h"
#include "tile.h"
#include "helpers.h"

/**
 * @brief Character::Character
 * @param myX X window co-ordinate.
 * @param myY Y window co-ordinate.
 * @param t Type of character.
 */
Character::Character(int myX, int myY, TileType t)
    : uTile(makeTile(x,y,t,Up)),dTile(makeTile(x,y,t,Down)), lTile(makeTile(x,y,t,Left)), rTile(makeTile(x,y,t,Right))
{
  x=myX;
  y=myY;
  //myType=t;


 // uTile.Tile=Up;


    // The tiles above have to be constructed with the class above.
    // Why can't we rather just set their value in here using uTile = makeTile(...)?
}

/**
 * @brief Character::render Call the relevant tiles render function based on current direction.
 * @param t Texture object for rendering.
 * @param frame Frame number to render.
 */
void Character::render(Texture *t, int frame)
{
    int i;
    uTile.x=x;
    uTile.y=y;
    dTile.y=y;
    lTile.x=x;
    lTile.y=y;
    rTile.x=x;
    rTile.y=y;


    /*switch(dir){
         case Up:
             i = frame % uTile.myFrames.size();
            //t->
                    uTile.render(t,frame);

                    //uTile.myFrames[i].first, uTile.myFrames[i].second,uTile.w,uTile.h);
        case Down:
             i = frame % dTile.myFrames.size();
             t->render(x, y, dTile.myFrames[i].first, dTile.myFrames[i].second,dTile.w,dTile.h);
        case Left:
             i = frame % lTile.myFrames.size();
             t->render(x, y, lTile.myFrames[i].first, lTile.myFrames[i].second,lTile.w,lTile.h);
        case Right:
            i = frame % rTile.myFrames.size();
            t->render(x, y, rTile.myFrames[i].first, rTile.myFrames[i].second,rTile.w,rTile.h);

    }*/
    uTile.x=x;
    uTile.y=y;
    dTile.x=x;
    dTile.y=y;
    if(dir == Up)
    {
        uTile.render(t,frame);

    }
    else if(dir == Down)
    {
     dTile.render(t,frame);
    }
    else if(dir == Left)
    {
     lTile.render(t,frame);
    }
    else if(dir == Right)
    {
     rTile.render(t,frame);
    }
}
/**
 * @brief Character::getNextPosition Calculates the position of the character based on direction.
 * @return SDL_Rect of the position and dimensions (in pixels) of the character.
 */
SDL_Rect Character::getNextPosition()
{

}
/**
 * @brief Character::handle_event Handles the SDL Events for Arrow Keypresses
 * @param e SDL_Event to check.
 */
void Character::handle_event(const SDL_Event &e)
{
    if( e.type == SDL_KEYDOWN && e.key.repeat == 0 )
    {
        //Adjust the velocity
        switch( e.key.keysym.sym )
        {
            case SDLK_UP:    dir = Up; break;
            case SDLK_DOWN:  dir = Down; break;
            case SDLK_LEFT:  dir = Left; break;
            case SDLK_RIGHT: dir = Right; break;
        }
    }
}
